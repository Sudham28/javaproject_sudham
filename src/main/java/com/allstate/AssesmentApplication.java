package com.allstate;

import com.allstate.dto.entities.PaymentDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class AssesmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssesmentApplication.class, args);
		PaymentDTO paymentDTO = new PaymentDTO(1,new Date(),"D",1000,1);
		System.out.printf(paymentDTO.toString());
	}

}
