package com.allstate.dal;

import com.allstate.dto.entities.PaymentDTO;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentRepository implements IPaymentRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public int rowCount() {
        Query query = new Query();
        long count = mongoTemplate.count(query, PaymentDTO.class);
        return (int)count;
    }

    @Override
    public PaymentDTO findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        PaymentDTO paymentDTO = mongoTemplate.findOne(query, PaymentDTO.class);
        return paymentDTO;
    }

    @Override
    public List<PaymentDTO> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<PaymentDTO> payments = mongoTemplate.find(query, PaymentDTO.class);
        return payments;
    }

    @Override
    public List<PaymentDTO> findAllPayments() {
        Query query = new Query();
        List<PaymentDTO> payments = mongoTemplate.findAll(PaymentDTO.class);
        return payments;
    }

    @Override
    public boolean savePayment(PaymentDTO paymentDTO) {
        boolean isSuccess = false;
        PaymentDTO paymet  = mongoTemplate.save(paymentDTO);
        if(paymet!=null)
            isSuccess =true;
        return isSuccess;
    }

    @Override
    public boolean updatePayment(PaymentDTO paymentDTO) {
        Query query = new Query();
        Update update = new Update();
        update.set("paymentDate", paymentDTO.getPaymentDate());
        update.set("type", paymentDTO.getType());
        update.set("amount", paymentDTO.getAmount());
        update.set("custid", paymentDTO.getCustid());
        query.addCriteria(Criteria.where("id").is(paymentDTO.getId()));
        UpdateResult updateResult = mongoTemplate.updateFirst(query,update, PaymentDTO.class);
        return updateResult.getMatchedCount()>0 ? true : false;
    }
}
