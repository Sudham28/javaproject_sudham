package com.allstate.bal;

import com.allstate.dal.IPaymentRepository;
import com.allstate.dto.entities.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements  IPaymentService {

    @Autowired
    IPaymentRepository iPaymentRepository;

    @Override
    public int rowCount()   {
        try{
            return iPaymentRepository.rowCount();
        }
        catch (Exception ex){
            throw ex;
       }
    }

    @Override
    public PaymentDTO findById(int id) {
       return iPaymentRepository.findById(id);
    }

    @Override
    public List<PaymentDTO> findByType(String type) {
       return iPaymentRepository.findByType(type);
    }

    @Override
    public List<PaymentDTO> findAllPayments() {
       return iPaymentRepository.findAllPayments();
    }

    @Override
    public boolean savePayment(PaymentDTO paymentDTO) {
        return iPaymentRepository.savePayment(paymentDTO);
    }

    @Override
    public boolean updatePayment(PaymentDTO paymentDTO) {
        boolean isSuccess = false;
        if(paymentDTO.getId()!=0) {
            PaymentDTO payment = iPaymentRepository.findById(paymentDTO.getId());
            if(payment!=null) {
                payment.setAmount(paymentDTO.getAmount());
                payment.setCustid(paymentDTO.getCustid());
                payment.setPaymentDate(paymentDTO.getPaymentDate());
                payment.setType(paymentDTO.getType());
                isSuccess = iPaymentRepository.updatePayment(paymentDTO);
            }

        }
        return isSuccess;
    }
}
