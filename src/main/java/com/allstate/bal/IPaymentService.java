package com.allstate.bal;

import com.allstate.dto.entities.PaymentDTO;

import java.util.List;

public interface IPaymentService {
    int rowCount();
    PaymentDTO findById(int id);
    List<PaymentDTO> findByType(String Type);
    List<PaymentDTO> findAllPayments();
    boolean savePayment(PaymentDTO paymentDTO);
    boolean updatePayment(PaymentDTO paymentDTO);
}
