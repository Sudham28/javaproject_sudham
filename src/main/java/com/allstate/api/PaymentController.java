package com.allstate.api;

import com.allstate.bal.IPaymentService;

import com.allstate.dto.entities.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("payment")
public class PaymentController {
    @Autowired
    private IPaymentService iPaymentService;

    @RequestMapping(value = "status", method = RequestMethod.GET)
    public String testAPI(){
        return "Payment API running successfully";
    }

    @RequestMapping(value = "rowcount", method = RequestMethod.GET)
    public int getPaymentCount()
    {
        try {
            return iPaymentService.rowCount();
        }
        catch(Exception ex){
            System.out.printf("Error occured in getPaymentCount API : Error discription:%s", ex.getStackTrace());
            throw ex;
        }
    }

    @RequestMapping(value = "findById/{id}", method =  RequestMethod.GET)
    public PaymentDTO findPaymentById(@PathVariable("id") int id){
        return iPaymentService.findById(id);
    }

    @RequestMapping(value = "findByType/{type}", method = RequestMethod.GET)
    public List<PaymentDTO> findPaymentByType(@PathVariable("type") String  type){
        return iPaymentService.findByType(type);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<PaymentDTO> findAllPayments(){
        return iPaymentService.findAllPayments();
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public boolean save(@RequestBody PaymentDTO paymentDTO){
        return  iPaymentService.savePayment(paymentDTO);
    }

    @RequestMapping(value = "update", method = RequestMethod.PUT)
    public boolean update(@RequestBody PaymentDTO paymentDTO){
        return iPaymentService.updatePayment(paymentDTO);
    }
}
