package com.allstate.dto.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class PaymentDTO {

    public PaymentDTO() {
    }

    public PaymentDTO(int id, Date paymentDate, String type, double amount, int custid) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custid = custid;
    }

    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custid=" + custid;
    }
}
