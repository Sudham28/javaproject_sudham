package com.allstate.dal;

import com.allstate.dto.entities.PaymentDTO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentRepositoryTest {

    @Autowired
    private IPaymentRepository iPaymentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;


    @BeforeAll
    void initiate(){
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 2);
        boolean isPaymentSaved = iPaymentRepository.savePayment(paymentDTO);
    }

    @AfterAll
    void  tearDown() {
        if( mongoTemplate.getCollectionName(PaymentDTO.class) != null) {
            String collectionName = mongoTemplate.getCollectionName(PaymentDTO.class);
            mongoTemplate.dropCollection(collectionName);
        }
    }

    @Test
    void savePayment_is_success() {
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "D", 1000, 2);
        boolean isPaymentSaved = iPaymentRepository.savePayment(paymentDTO);
        assertTrue("Payment saved successfully", isPaymentSaved);
    }

    @Test
    void rowCount() {
        PaymentDTO paymentDTO = new PaymentDTO();
        int paymentCount = iPaymentRepository.rowCount();
        assertEquals("got the expected payment row count",1,paymentCount);
    }

    @Test
    void findById() {
        PaymentDTO paymentDTO = iPaymentRepository.findById(123);
        assertEquals("Payment object is matching", 1000.0, paymentDTO.getAmount());
    }

    @Test
    void findByType() {
        List<PaymentDTO> payments = iPaymentRepository.findByType("C");
        assertEquals("Payment object is matching", 1, payments.size());
    }

    @Test
    void findAllPayments() {
        List<PaymentDTO> payments = iPaymentRepository.findAllPayments();
        assertEquals("Payment object is matching", 1, payments.size());
    }

    @Test
    void updatePayment() {
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 1);
        boolean isPaymentSaved = iPaymentRepository.updatePayment(paymentDTO);
        assertTrue("Payment saved successfully", isPaymentSaved);
    }
}