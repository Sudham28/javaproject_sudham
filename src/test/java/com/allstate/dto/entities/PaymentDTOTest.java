package com.allstate.dto.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentDTOTest {

    private PaymentDTO paymentDTO;

    @BeforeEach
    void setUp(){
         paymentDTO = new PaymentDTO(123,new Date(),"C",1000, 4);
    }

    @Test
    void getId() {
        assertEquals(123,paymentDTO.getId());
    }


    @Test
    void getPaymentDate() {
        assertEquals(new Date(), paymentDTO.getPaymentDate());
    }

    @Test
    void getType() {
        assertEquals("C", paymentDTO.getType());
    }

    @Test
    void getAmount() {
        assertEquals(1000, paymentDTO.getAmount());
    }

    @Test
    void getCustid() {
    }

}