package com.allstate.bal;

import com.allstate.dto.entities.PaymentDTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentServiceTest {

    @Autowired
    IPaymentService iPaymentService;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeAll
    void initiate(){
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 2);
        boolean isPaymentSaved = iPaymentService.savePayment(paymentDTO);
    }

    @Test
    void savePayment() {
        PaymentDTO paymentDTO = new PaymentDTO(124, new Date(), "D", 1000, 2);
        boolean isPaymentSaved = iPaymentService.savePayment(paymentDTO);
        assertTrue("Payment saved successfully", isPaymentSaved);
    }

    @Test
    void updatePayment() {
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 1);
        boolean isPaymentSaved = iPaymentService.updatePayment(paymentDTO);
        assertTrue("Payment saved successfully", isPaymentSaved);
    }

    @Test
    void rowCount() {
        int count = iPaymentService.rowCount();
        assertTrue("Payment count",count>0);
    }

    @Test
    void findByType() {
        List<PaymentDTO> payments = iPaymentService.findByType("C");
        assertEquals("Payment object is matching", 1, payments.size());
    }

    @Test
    void findById() {
        String pattern = ("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        PaymentDTO paymentDTO = iPaymentService.findById(123);
        String date = simpleDateFormat.format(paymentDTO.getPaymentDate());
        assertEquals("Payment object is matching", 1000.0, paymentDTO.getAmount());
        assertEquals("Check the date format", date, simpleDateFormat.format(paymentDTO.getPaymentDate()));
    }

    @Test
    void findAllPayments() {
        List<PaymentDTO> payments = iPaymentService.findAllPayments();
        assertEquals("Payment object is matching", 1, payments.size());
    }

    @AfterAll
    void  tearDown() {
        if( mongoTemplate.getCollectionName(PaymentDTO.class) != null) {
            String collectionName = mongoTemplate.getCollectionName(PaymentDTO.class);
            mongoTemplate.dropCollection(collectionName);
        }
    }
}