package com.allstate.bal;

import com.allstate.dto.entities.PaymentDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.AssertionErrors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.util.AssertionErrors.assertTrue;

public class PaymentServiceTestMock {

    @Mock
    IPaymentService iPaymentService;


    @BeforeEach
    void  setUp(){
        initMocks(this);
    }

    @Test
    void rowCount(){
        doReturn(2).when(iPaymentService).rowCount();
        assertEquals(2, iPaymentService.rowCount());
    }


    @Test
    void savePayment_passed() {
        doReturn(true).when(iPaymentService).savePayment(null);
        assertTrue("Payment saved successfully", iPaymentService.savePayment(null));
    }

    @Test
    void updatePayment() {
        doReturn(true).when(iPaymentService).updatePayment(null);
        assertTrue("Payment saved successfully", iPaymentService.updatePayment(null));
    }


    @Test
    void findByType() {
        List<PaymentDTO> payments = new ArrayList<PaymentDTO>();
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 2);
        payments.add(paymentDTO);
        doReturn(payments).when(iPaymentService).findByType("");
        AssertionErrors.assertEquals("Payment object is matching", 1, payments.size());
    }

    @Test
    void findById() {

        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 2);
        doReturn(paymentDTO).when(iPaymentService).findById(123);
        AssertionErrors.assertEquals("Payment object is matching", 1000.0, paymentDTO.getAmount());
    }

    @Test
    void findAllPayments() {
        List<PaymentDTO> payments = new ArrayList<PaymentDTO>();
        PaymentDTO paymentDTO = new PaymentDTO(123, new Date(), "C", 1000, 2);
        PaymentDTO paymentDTO2 = new PaymentDTO(123, new Date(), "C", 1000, 2);
        payments.add(paymentDTO);
        payments.add(paymentDTO2);
        doReturn(payments).when(iPaymentService).findAllPayments();
        AssertionErrors.assertEquals("Payment object is matching", 2, payments.size());
    }
}


