package com.allstate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class AssesmentApplicationTests {


	int port =8080;

	@Bean
	public RestTemplate restTemplates(){
		return new RestTemplate();
	}

	@Test
	void contextLoads() {

	}

	@Test
	public void testAPIShouldReturnTheMessage(){
		RestTemplate restTemplate = new RestTemplate();
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/payment/status",
				String.class)).contains("Payment API running successfully");
	}

}
